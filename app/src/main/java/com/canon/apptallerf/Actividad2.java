package com.canon.apptallerf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Actividad2 extends AppCompatActivity {

    Button mapa, calcu, equipo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad2);

        mapa = findViewById(R.id.btMapa);
        calcu = findViewById(R.id.btnCalcu);
        equipo = findViewById(R.id.btnEquipos);

        mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Actividad2.this, MapaActivity.class);
                startActivity(i);
            }
        });
        calcu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Actividad2.this, CalculadoraActivity.class);
                startActivity(i);
            }
        });
        equipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Actividad2.this, EquiposActivity.class);
                startActivity(i);
            }
        });
    }
}