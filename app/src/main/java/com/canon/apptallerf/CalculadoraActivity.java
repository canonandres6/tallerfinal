package com.canon.apptallerf;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CalculadoraActivity extends AppCompatActivity implements View.OnClickListener {
    EditText num1, num2;
    Button suma, resta,mul, div;
    TextView respuesta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        num1 = findViewById(R.id.n1);
        num2 = findViewById(R.id.n2);

        suma = findViewById(R.id.sumar);
        resta = findViewById(R.id.Restar);
        mul = findViewById(R.id.Mul);
        div = findViewById(R.id.Div);
        respuesta=findViewById(R.id.respuesta);

        suma.setOnClickListener(this);
        resta.setOnClickListener(this);
        mul.setOnClickListener(this);
        div.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {

        String n1 = num1.getText().toString();
        String n2 = num2.getText().toString();

        int valor1 = Integer.parseInt(n1);
        int valor2 = Integer.parseInt(n2);

        int resul =0;

        switch (view.getId()){
            case R.id.sumar:
                resul = valor1+valor2;
                break;
            case R.id.Restar:
                resul = valor1-valor2;
                break;
            case R.id.Mul:
                resul = valor1*valor2;
                break;
            case R.id.Div:
                resul = valor1/valor2;
                break;
        }
        respuesta.setText(""+resul);
    }

}